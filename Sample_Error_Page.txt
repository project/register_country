<?php
  echo '<div style="font-size: 125%;">';
  $addr = $_SERVER['REMOTE_ADDR'];
  $country = ip2cc_get_country($addr);
  echo "<p>I'm sorry but your IP address ($addr) is registered to ";
  if ($country) { echo $country->country_name; }
  else { echo "an unknown country"; }
  echo ".</p><p>The owners of this site prefer to restrict membership to the United States.</p>";
  echo '<p>If you are an American temporarily using a foreign internet connection and wish to register, please <a href="/contact">contact us</a> and explain your situation. We will get back to you as soon as we can.</p>';
  echo '</div>';
?>